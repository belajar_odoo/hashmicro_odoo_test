# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from dateutil.relativedelta import relativedelta

class WorkOrderBookingOrder(models.Model):
    _name = 'work.order'

    name = fields.Char(string='name', related='wo_number')
    wo_number = fields.Char(string='Work Order Number')
    booking_order_ref =  fields.Many2one('sale.order', string='Booking Order Reference')
    team_id = fields.Many2one('service.team', required=True, string='Team')
    team_leader_id = fields.Many2one('res.users', string='Team Leader', required=True)
    team_member_id = fields.Many2many('res.users', string='Team Member')
    planned_start = fields.Datetime(string='Planned Start', required=True)
    planned_end = fields.Datetime(string='Planned End', required=True)
    date_start = fields.Datetime(string='Date Start')
    date_end = fields.Datetime(string='Date End')
    state = fields.Selection([
            ('pending','Pending'),
            ('progress', 'In Progress'),
            ('done', 'Done'),
            ('cancel', 'Cancelled'),
        ], default='pending')
    notes = fields.Text('Notes')

    def action_start_work(self):
        self.state = 'progress'
        self.date_start = fields.Datetime.now()

    def action_end_work(self):
        self.state = 'done'
        self.date_end = fields.Datetime.now()


    def action_progress_work(self):
        self.state = 'progress'
        self.date_start = ''


    def action_cancel_work(self):
        self.state = 'cancel'
        view_id = self.env.ref('booking_order_arief_31031994.act_cancel_wizard_popup').id
        return{
            'type': 'ir.actions.act_window',
            'res_model': 'cancel.wizard',
            'view_id': False,
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
        }

