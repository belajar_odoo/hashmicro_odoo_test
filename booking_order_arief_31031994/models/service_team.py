# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime
from dateutil.relativedelta import relativedelta

class ServiceTeamBookingOrder(models.Model):
    _name = 'service.team'

    name = fields.Char(string='name', related='team_name')
    team_name = fields.Char(required=True, string='Team Name')
    team_leader = fields.Many2one('res.users', string='Team Leader', required=True)
    team_member = fields.Many2many('res.users', string='Team Member')
