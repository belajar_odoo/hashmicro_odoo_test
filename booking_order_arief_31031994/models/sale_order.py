# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo.exceptions import UserError, ValidationError

class SaleOrder(models.Model):
    _inherit = 'sale.order'

    is_booking_order = fields.Boolean(string='Is Booking', default=True)
    team = fields.Many2one('service.team', string='Service Team')
    team_leader = fields.Many2one('res.users', string='Team Leader')
    team_member = fields.Many2many('res.users', string='Team Member')
    booking_start = fields.Datetime(string='Booking Start')
    booking_end = fields.Datetime(string='Booking End')

    @api.onchange('team')
    def onchange_team(self):
        self.team_leader = self.team.team_leader
        self.team_member = self.team.team_member


    def action_check(self):
        for wo in self.env['work.order'].search([('team_id','=',self.team.id)]):
            if wo.state == 'progress' and wo.date_start <= self.booking_start <= wo.date_end and wo.date_start <= self.booking_end <= wo.date_end:
                raise ValidationError(_('Team already has work order during that period on %s')% wo.booking_order_ref.name)
            else:
                raise ValidationError(_('Team is Avaliable for Booking'))

    @api.multi
    def action_confirm(self):
        confirm = super(SaleOrder, self).action_confirm()
        for wo in self.env['work.order'].search([('team_id','=',self.team.id)]):
            if wo.state == 'progress' and wo.date_start <= self.booking_start <= wo.date_end and wo.date_start <= self.booking_end <= wo.date_end:
                raise ValidationError(_('Team is Avaliable for Booking'))

            else:
                print self.team_leader.id, self.team_id.id
                self.env['work.order'].create({
                    'wo_number':self.name,
                    'team_id':self.team.id,
                    'team_leader_id':self.team_leader.id,
                    'planned_start':self.booking_start,
                    'planned_end':self.booking_end,
                    'state': 'pending'
                    })
        
            return confirm
