# -*- coding: utf-8 -*-
{
    'name': "Booking Order arief_31031994",

    'summary': """
        Test Hashmicro Arief Afandy""",

    'description': """
        Long description of module's purpose
    """,

    'author': "Arief Afandy from ERPI",
    'website': "fandypedia.blogspot.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/10.0/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'sale'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/service_team_view.xml',
        'views/booking_order_view.xml',
        'views/work_order_view.xml',
        'wizard/cancel_wizard_view.xml',
        'report/work_order_report.xml',
        'report/template_report.xml',
        'views/menuitem.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}