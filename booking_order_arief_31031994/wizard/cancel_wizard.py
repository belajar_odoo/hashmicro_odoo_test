# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import RedirectWarning, UserError, ValidationError, AccessError



class CancelWizard(models.TransientModel):
    _name = 'cancel.wizard'

    username = fields.Char(string='username')
    pwd_invoice = fields.Char(string='password')
    notes = fields.Text('Notes')

    
    def wizard_confirm(self):
        wo_obj = self.env['work.order']
        context = dict(self._context or {})
        print ".....", self.notes

        for wo in wo_obj.browse(context.get('active_ids')):
            wo.write({'notes':self.notes})
            print wo.name, wo.notes